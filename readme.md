## Stream your audio content easy with latest [liquidsoap 2.1.4](https://github.com/savonet/liquidsoap/releases/tag/v2.1.4) extended by extras and [icecast-kh 2.4.0-kh16](https://github.com/karlheyes/icecast-kh/releases/tag/icecast-2.4.0-kh16)

[![pipeline status](https://gitlab.com/gradio.lv/docker-stream/badges/master/pipeline.svg)](https://gitlab.com/gradio.lv/docker-stream/-/commits/master)

### This project glues together [liquidsoap](https://hub.docker.com/repository/docker/rolla/liquidsoap) with [icecast](https://hub.docker.com/repository/docker/rolla/icecast-kh)

---
# links
#### [Docker hub liquidsoap](https://hub.docker.com/repository/docker/rolla/liquidsoap)
#### [Docker hub icecast-kh](https://hub.docker.com/repository/docker/rolla/icecast-kh)
#### [Gitlab gradio.lv docker-liquidsoap](https://gitlab.com/gradio.lv/docker-liquidsoap)
#### [Gitlab gradio.lv docker-icecast-kh](https://gitlab.com/gradio.lv/docker-icecast-kh)

# liquidsoap docker commands
```bash
# see version
docker run rolla/liquidsoap 'set("init.force_start", true)'

# mount your custom liq config file
docker run --rm -p 1234:1234 -v $(pwd)/config/radio.liq:/home/liquidsoap/config/radio.liq rolla/liquidsoap
```
# build
it's multi stage build see: [.gitlab-ci.yml](./.gitlab-ci.yml)

---
#### Adding music
Copy your files/folders to [music](content/music) directory

---
#### To start with logs
```bash
docker-compose up
```

#### To start in background without logs
```bash
docker-compose up -d
```

# icecast admin ui
default credentials:
```
username: admin
password: hackme
```

Open in browser: http://admin:hackme@localhost:8000/admin.html

---
#### To build
```bash
time docker-compose -f docker-compose-build.yml build
```

# liquidsoap dockerfile defaults

| KEY              | VALUE |
| :--------------: | :---------------------: |
| ENTRYPOINT | `liquidsoap` |
| CMD | `/home/liquidsoap/config/radio.liq` |

```bash
# to override
docker run --rm --entrypoint sh rolla/liquidsoap -c "sleep 10"
```

#!/bin/bash

set -e

# rolla - goes to docker hub
CI_REGISTRY_IMAGE=rolla

LIQ_VERSION="2.1.4"
IC_VERSION="2.4.0-kh16"

LIQ_IMAGE="${CI_REGISTRY_IMAGE}/liquidsoap"
LIQ_CONTAINER="rolla-liquidsoap"

IC_IMAGE="${CI_REGISTRY_IMAGE}/icecast-kh"
IC_CONTAINER="rolla-icecast-kh"

echo ""
echo "Current $0 LIQ_VERSION: ${LIQ_VERSION}"
echo "Current $0 IC_VERSION: ${IC_VERSION}"
echo "Current liquidsoap docker-compose-build.yml: $(cat docker-compose-build.yml | grep -m 1 LIQ_VERSION)"
echo "Current icecast docker-compose-build.yml: $(cat docker-compose-build.yml | grep -m 1 IC_VERSION)"
echo ""
echo "Update $0 and docker-compose-build.yml LIQ_VERSION and IC_VERSION arguments to latest versions"
echo ""
read -p "Restart this script and press enter to continue"
echo ""

echo "Removing liquidsoap containers and images"
docker rm -f ${LIQ_CONTAINER} || true
docker rmi -f ${LIQ_IMAGE}:${LIQ_VERSION}
docker rmi -f ${LIQ_IMAGE}:latest

echo "Removing icecast containers and images"
docker rm -f ${IC_CONTAINER} || true
docker rmi -f ${IC_IMAGE}:${IC_VERSION}
docker rmi -f ${IC_IMAGE}:latest

echo "Cleaning system"
echo "y" | docker system prune

echo "Building new image"
time docker-compose -f docker-compose-build.yml build

# export DOCKER_BUILDKIT=1
# docker build -t rolla/liquidsoap-base-linux -f services/liquidsoap/base/linux.Dockerfile .
# docker build -t rolla/liquidsoap-base-opam -f services/liquidsoap/base/opam.Dockerfile .
# docker build --build-arg LIQ_VERSION=1.4.3 -t rolla/liquidsoap-base-install:1.4.3 -f services/liquidsoap/base/install.Dockerfile .
# docker build --build-arg LIQ_VERSION=1.4.3 -t rolla/liquidsoap:1.4.3 -f services/liquidsoap/version/Dockerfile .
#
# docker build -t rolla/liquidsoap:release -f services/liquidsoap/release/Dockerfile .
# docker build -t rolla/liquidsoap:master -f services/liquidsoap/latest/Dockerfile .
# docker build -t rolla/icecast-kh -f services/icecast/Dockerfile .
# docker run --rm -it --entrypoint="" rolla/liquidsoap:1.4.3 bash

echo ""
echo "Test your image with: docker-compose up"
echo ""
echo "Now you can push to docker hub"
echo ""
echo "liquidsoap push and retag:"
echo "docker push ${CI_REGISTRY_IMAGE}/liquidsoap:${LIQ_VERSION}"
echo "retag as latest"
echo "docker tag ${CI_REGISTRY_IMAGE}/liquidsoap:${LIQ_VERSION} ${CI_REGISTRY_IMAGE}/liquidsoap:latest"
echo "docker push ${CI_REGISTRY_IMAGE}/liquidsoap:latest"
echo ""
echo "icecast push and retag:"
echo "docker push ${CI_REGISTRY_IMAGE}/icecast-kh:${IC_VERSION}"
echo "retag as latest"
echo "docker tag ${CI_REGISTRY_IMAGE}/icecast-kh:${IC_VERSION} ${CI_REGISTRY_IMAGE}/icecast-kh:latest"
echo "docker push ${CI_REGISTRY_IMAGE}/icecast-kh:latest"
echo ""
